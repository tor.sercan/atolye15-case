import { File, List, NOT_FOUND_INDEX } from './types';
import * as OperationChecks from './operation-checks';

// IMPROVEMENT: I'm mutating the original list, meaning I'm producing side effects.
export default function move(list: List[], sourceId: string, destinationId: string): List[] {
  const sourceFileIndexTest = (file: File) => file.id === sourceId;

  let file: File | undefined;
  let fileIndex = NOT_FOUND_INDEX;
  let folderIndex = NOT_FOUND_INDEX;

  list.forEach((folder, index) => {
    folderIndex = folder.id === destinationId ? index : NOT_FOUND_INDEX;
    OperationChecks.checkSourceIsFile(folder.id, sourceId);
    // another foreach loop would increase time complexity, findIndex will stop searching as soon as the test is correct
    fileIndex = folder.files.findIndex(sourceFileIndexTest);
    if (fileIndex !== NOT_FOUND_INDEX) {
      file = folder.files.splice(fileIndex, 1).shift();
    }
  });

  OperationChecks.checkDestinationIsFolder(folderIndex);
  // finally insert it into the folder if the file is found
  if (file) {
    list[folderIndex].files.push(file);
  }

  return list;
}
