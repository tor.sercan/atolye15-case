import { NOT_FOUND_INDEX } from './types';
import MoveError from './error';

function checkSourceIsFile(folderId: string, sourceId: string): void {
  if (folderId === sourceId) {
    throw new MoveError('You cannot move a folder');
  }
}

function checkDestinationIsFolder(folderIndex: number): void {
  if (folderIndex === NOT_FOUND_INDEX) {
    throw new MoveError('You cannot specify a file as the destination');
  }
}

export { checkSourceIsFile, checkDestinationIsFolder };
