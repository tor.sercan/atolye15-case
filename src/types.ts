type File = {
  id: string;
  name: string;
};

const NOT_FOUND_INDEX = -1;

type List = {
  id: string;
  name: string;
  files: File[];
};

export { File, List, NOT_FOUND_INDEX };
