export default class MoveError extends Error {
  constructor(message: string) {
    super();
    Error.call(this);
    Error.captureStackTrace(this);
    this.message = message;
  }
}
